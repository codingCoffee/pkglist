
# pkglist

A list of packages, arranged for modularity for making replicable systems

```sh
modules/$MODULE_NAME/$OS/pkg.txt
```

