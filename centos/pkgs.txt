# Essentials #
##############
stow # to sync dotfiles
zsh # customizable interactive shell
ripgrep # grep replacement in rust
htop # interactive process viewer
