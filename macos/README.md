
To install all formulae:

```sh
awk '{print $1}' formulae.txt | xargs brew install
```

To install all casks:

```sh
awk '{print $1}' casks.txt | xargs brew cask install
```

To download all docker images:

```sh
awk '{print $1}' docker.txt | xargs -n 1 docker pull
```
