# Essentials #
##############
stow # to sync dotfiles
zsh # customizable interactive shell

# Compile essentials for pyenv #
################################
build-essential
curl
git
libbz2-dev
libffi-dev
liblzma-dev
libncurses5-dev
libncursesw5-dev
libreadline-dev
libsqlite3-dev
libssl-dev
llvm
make
python-openssl
tk-dev
wget
xz-utils
zlib1g-dev

# MySQL Essentials #
####################
libmysqlclient-dev

# Containerization #
####################
apt-transport-https
ca-certificates
containerd.io
curl
docker-ce
docker-ce-cli
gnupg-agent
software-properties-common

